"""
This file is part of amigo_invisible.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import random
import smtplib
from email.mime.text import MIMEText

import settings


def open_connection():
    global session
    session = smtplib.SMTP_SSL(settings.SMTP_SERVER, 465)
    session.ehlo()
    session.login(settings.SENDER, settings.SENDER_PASSWORD)


def close_connection():
    global session
    session.quit()


def send_email(template, dest, friend):
    global session

    text = template.format(friend=friend)

    msg = MIMEText(text, 'html')
    msg['Subject'] = 'Amigo invisible 2019'
    msg['From'] = settings.SENDER
    msg['To'] = dest

    session.sendmail(settings.SENDER, [dest], msg.as_string())


open_connection()
template = open('template.txt', 'rb').read().decode("utf8")
friends_list = settings.EMAIL_LIST
random.shuffle(friends_list)
for i in range(len(friends_list)):
    send_email(template, friends_list[i][1],
               friends_list[(i + 1) % len(friends_list)][0])

close_connection()
print("Messages delivered correctly")
